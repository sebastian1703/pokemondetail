var app = angular.module("myApp", []);
app.controller("ListController", function($scope, $http, booksApi) {
    $scope.modalDetailPokemon = false;
    booksApi.getPokemon().then(function(res){
        console.log(res);
        $scope.pokemon = res.data.results;
        for(var i in $scope.pokemon){            
            $scope.getIdPokemon(i); // Untuk mendapatkan idPokemon nya
        }
    });

    $scope.getIdPokemon = function(i){
        booksApi.getDetailPokemon($scope.pokemon[i].url).then(function(res){
            console.log(res.data);
            $scope.pokemon[i].pokemonId = res.data.id;            
        });  
    }

    $scope.getPokemonDetail = function(url){
        console.log('Masuk Detail');
        booksApi.getDetailPokemon(url).then(function(res){ // Get detail pokemon
            console.log(res.data);
            $scope.detailedPokemon = res.data;            
        });      
        $scope.openModalPokemonDetail();
    }   

    $scope.openModalPokemonDetail = function(){    
        var modal = document.getElementById('myModal');
        modal.classList.add('show');
        modal.style.display = 'block';
        document.body.classList.add('modal-open');
    }    
    
    $scope.closeModalPokemonDetail = function() {
        var modal = document.getElementById('myModal');
        modal.classList.remove('show');
        modal.classList.add('hide');
        modal.style.display = 'none';
        document.body.classList.remove('modal-open');
    };    
  
});

app.factory("booksApi", function($http){

    var _getPokemon = function() {
        return $http.get('https://pokeapi.co/api/v2/pokemon?offset=0&limit=20');
    };

    var _getDetailPokemon = function(urlDetail){
        return $http.get(urlDetail);
    }

    return{
        getPokemon: _getPokemon,
        getDetailPokemon: _getDetailPokemon
    }
    
    
});
